# Background Video Autoplayer

This module provides the assorted collection of hook implementations to make a
video automatically play in the background in Chrome and Firefox.

Note: due to browser issues it will not work in Safari.

## Dependencies

In order for this to work the latest patch from the following issue must be
added to core:

* https://www.drupal.org/project/drupal/issues/3042423
